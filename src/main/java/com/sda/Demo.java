package com.sda;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/*
Aveti o lista cu numere de dimensiune n+1 cu numerele de la 1 la n, in care unul din ele apare de 2 ori.
Afisati numarul respectiv.
 */
public class Demo {
    public static void main(String[] args) {

        List<Integer> numbers = Stream.iterate(1, n->n<=40, n->n+1).collect(Collectors.toList());
        numbers.add(new Random().nextInt(40)+1);
        Collections.shuffle(numbers);

        numbers.forEach(x-> System.out.print(x+" "));
        System.out.println();

        //varianta 1 - comparam  fiecare element in parte. In cazul in care sunt egale acel numar este duplicat.
        // Complexitate O(n*n)
        int duplicate = findDuplicate2For(numbers);
        System.out.println(duplicate==-1 ? "Not found":"Duplicate is "+duplicate);

        //varianta 2 - parcurgem lista si punem numerele intr-un set. Metoda add returneaza false atunci cand un numar exista deja in set
        //complexitate O(n)
        duplicate = findDuplicateUsingSet(numbers);
        System.out.println(duplicate==-1 ? "Not found":"Duplicate is "+duplicate);

        //varianta 3 - cu array: cream un array de boolean dimensiunea listei(Stim ca elemenetele sunt 1-n). Cand gasim prima oara un element in lista, punem pozitia respectiva true.
        // daca pozitia respecitva e deja true, inseamna ca avem duplicate.
        // varianta aceasta o putem folosi si in probleme care trebuie sa stim numarul aparitiilor pentru fiecare element din lista
        //Complexitate O(n)
        duplicate = findDuplicateWithArray(numbers);
        System.out.println(duplicate==-1 ? "Not found":"Duplicate is "+duplicate);

        //varianta 4 - diferenta sumelor. Suma numerelor din lista  minus suma numerelor 1-n.
        //Complexitate O(n)
        duplicate =findDuplicateSumDifference(numbers);
        System.out.println("Duplicate is " + duplicate);

        //varianta 5 - varianta 4 cu streams :)
        //Complexitate O(n)
        duplicate =findDuplicateSumDifferenceUsingStream(numbers);
        System.out.println("Duplicate is " + duplicate);

        //varianta 6 - sort si comparam elementele consecutive. Dezavantaj ar fi ca modificam colectia initiala. Evident am putea face o copie intai. Dar complexitatea oricum este mai mare decat la variantele 2-5
        //Complexitate O(n log(n))  data de sort. Chiar daca noi scriem o singura linie pentru a sorta, algoritmul de sortare nu are complexitate liniara O(n) ci O(n log(n))
        duplicate =findDuplicateUsingSort(numbers);
        System.out.println("Duplicate is " + duplicate);
    }

    private static int findDuplicateUsingSort(List<Integer> numbers) {
        Collections.sort(numbers);
        for (int i = 0; i < numbers.size()-1; i++) {
            if(numbers.get(i)==numbers.get(i+1))
                return numbers.get(i);
        }
        return -1;
    }

    private static int findDuplicateSumDifferenceUsingStream(List<Integer> numbers) {
        // am folosit 2 variante pentru sum doar pentru exemplificare, o data sum() din IntStream, in partea a 2 a,  metoda reduce. Evident intr-un proiect suntem consecventi, am folosi accelasi mod in ambele parti
        return numbers.stream().mapToInt(i->i).sum() - Stream.iterate(1,n-> n<numbers.size(),n->n+1).reduce(0, Integer::sum);
    }

    private static int findDuplicateSumDifference(List<Integer> numbers) {
        int sum=0;
        for(int n:numbers){
            sum+=n;
        }
        int sumN=0;
        for(int i=1;i<numbers.size();i++){
            sumN+=i;
        }
        // sau daca tineti minte de la matematica, suma numerelor de la 1-n  este n*(n+1)/2;
        return sum-sumN;
    }

    private static int findDuplicateWithArray(List<Integer> numbers) {
        boolean[] check = new boolean[numbers.size()];
        for (int i = 0; i < numbers.size(); i++) {
            if(check[numbers.get(i)]){
                return numbers.get(i);
            } else{
                check[numbers.get(i)]=true;
            }
        }
        return -1;
    }

    private static int findDuplicateUsingSet(List<Integer> numbers) {
        Set<Integer> set = new HashSet<>();
        for(int n:numbers){
            if(!set.add(n))
                return n;
        }
        return -1;
    }

    private static int findDuplicate2For(List<Integer> numbers) {
        for(int i=0;i<numbers.size()-1;i++){
            for(int j=i+1;j<numbers.size();j++){
                if(numbers.get(i)==numbers.get(j)){
                    return numbers.get(i);
                }
            }
        }
        return -1;
    }
}
